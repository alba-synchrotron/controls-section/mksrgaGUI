======
mksrga
======


.. image:: https://img.shields.io/pypi/v/mksrga.svg
        :target: https://pypi.python.org/pypi/mksrga

.. image:: https://img.shields.io/travis/LluisG/mksrga.svg
        :target: https://travis-ci.org/LluisG/mksrga

.. image:: https://readthedocs.org/projects/mksrga/badge/?version=latest
        :target: https://mksrga.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




mksrga is a collection of tools and widgets for MKS Resuidual Gas Analyzers Microvision IP


* Free software: GPLv3+
* Documentation: https://mksrga.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `taurus-org/cookiecutter-taurus`_ template
(based on `audreyr/cookiecutter-pypackage`_).

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`taurus-org/cookiecutter-taurus`: https://github.com/taurus-org/cookiecutter-taurus
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
