import sys
from PyQt5 import Qt
from taurus.qt.qtgui.base import TaurusBaseWidget
from addMassWidget2 import PeakAddMass

class AddMassForm(Qt.QWidget):
    def __init__(self, model):
        Qt.QWidget.__init__(self, parent=None)

        self.v_layout = Qt.QVBoxLayout()

        for i in range(0, 10):
            myWidget=PeakAddMass(model, i)
            self.v_layout.addWidget(myWidget)


        self.setLayout(self.v_layout)
        # self.show()

if __name__ == "__main__":
    # Initialize a Qt application (Qt will crash if you do not do this first)
    app = Qt.QApplication(sys.argv)

    # instantiate the widget
    w = AddMassForm("comedor/mksRGA/1")
    # w.show()
    # Initialize the Qt event loop (and exit when we close the app)
    sys.exit(app.exec_())
