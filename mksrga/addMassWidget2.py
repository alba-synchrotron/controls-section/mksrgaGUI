import os
import sys
from PyQt5 import Qt
from PyQt5.QtWidgets import QMessageBox
from taurus.external.qt import uic, Qt

from taurus.qt.qtgui.base import TaurusBaseWidget
from taurus.qt.qtgui.input.tauruscombobox import TaurusAttrListComboBox
from taurus.qt.qtgui.button.taurusbutton import TaurusCommandButton
import tango

class PeakAddMass(Qt.QWidget):
    def __init__(self, model, widgetNumber, parent=None):
        Qt.QWidget.__init__(self, parent=parent)

        self.rga = tango.DeviceProxy(model)
        self.widgetNumber = widgetNumber

        self.rga=tango.DeviceProxy(model)

        self.maxmass=self.rga.read_attribute('MaxMass')
        self.addingMass=True

        self.nScan = Qt.QLabel()
        self.nScan.setToolTip("Channel")
        self.nScan.setText(str(self.widgetNumber))


        self.layout = Qt.QHBoxLayout(self)

        """
        "Structure of add mass: mass, detector, accuracy, gain"
        """
        self.setFixedWidth(400)
        self.massBox = Qt.QLineEdit()
        self.massBox.setValidator(Qt.QIntValidator())
        self.massBox.setMaxLength(3)
        self.massBox.setFixedWidth(35)
        self.massBox.setText("")
        self.massBox.setToolTip("Mass to scan")

        self.sensorsBox = TaurusAttrListComboBox()
        self.sensorsBox.setModel(model + '/Sensors')
        self.sensorsBox.setToolTip("Sensor to use for Scan")

        self.gainBox = TaurusAttrListComboBox()
        self.gainBox.setModel(model + '/Gains')
        self.gainBox.setToolTip("Gain to be used")

        self.accuracyCombo = Qt.QComboBox()
        self.accuracyCombo.setToolTip("Accuracy to use")
        for i in range(9):
            self.accuracyCombo.addItem(str(i))

        self.button = Qt.QPushButton()
        self.button.setText("Add Mass")
        self.button.setToolTip("Add this set to the scan")
        self.button.clicked.connect(self.addMass)

        self.layout.addWidget(self.nScan)
        self.layout.addWidget(self.massBox)
        self.layout.addWidget(self.sensorsBox)
        self.layout.addWidget(self.gainBox)
        self.layout.addWidget(self.accuracyCombo)
        self.layout.addWidget(self.button)

        # if disabled:
        #     self.disableWidgets(True)
        # else:
        #     self.disableWidgets(False)

        # self.panel.show()


    def addMass(self):
        massDevice=self.maxmass.value
        mass = int(self.massBox.text())
        if not mass or mass ==0:
            return
        if self.addingMass:
            if mass > massDevice:
                self.errorPoPuP()
            else:
                sensor = int(self.sensorsBox.currentIndex())
                gain = int(self.gainBox.currentIndex())
                accuracy =int( self.accuracyCombo.currentIndex())
                self.rga.command_inout("PeakScanAddMass",
                                       [mass, accuracy, gain, sensor])

                self.disableWidgets(True)
                self.button.setText("Remove Mass")
                self.addingMass=(False)
        else:
            self.button.setText("Add Mass")
            self.disableWidgets(False)
            self.rga.command_inout("peakScanRemoveMassWeight", mass)
            self.addingMass=(True)


    def disableWidgets(self, state):

        self.sensorsBox.setDisabled(state)
        self.accuracyCombo.setDisabled(state)
        self.massBox.setDisabled(state)
        self.accuracyCombo.setDisabled(state)
        self.gainBox.setDisabled(state)


    def enableWidgets(self, bool):

        self.sensorsBox.setDisabled(bool)
        self.accuracyCombo.setDisabled(bool)
        self.massBox.setDisabled(bool)
        self.accuracyCombo.setDisabled(bool)
        self.gainBox.setDisabled(bool)

    def errorPoPuP(self):

        popup = QMessageBox()
        popup.setWindowTitle("ERROR")
        popup.setText("Max mass supported by the device : {a} \n "
                      "Please check your settings.".format(a=self.maxmass.value))
        popup.setIcon(QMessageBox.Critical)
        popup.setStandardButtons(QMessageBox.Close)
        popup.exec_()





if __name__ == "__main__":
    # Initialize a Qt application (Qt will crash if you do not do this first)
    app = Qt.QApplication(sys.argv)
    # instantiate the widget
    f = PeakAddMass('comedor/mksRGA/1',0)
    f.show()
    sys.exit(app.exec_())
