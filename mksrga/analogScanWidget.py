import sys

from PyQt5 import Qt, QtCore

import taurus, taurus_pyqtgraph
import taurus.qt.qtgui

import tango
from taurus.qt.qtgui.input.tauruscombobox import TaurusAttrListComboBox
from filament_2 import FilamentManager

from PyQt5.QtWidgets import QMessageBox, QSplitter
from taurus.qt.qtgui.extra_guiqwt import taurustrend2d as t2d
from taurus.qt.qtgui.button.taurusbutton import TaurusCommandButton




class AnalogScanWidget(Qt.QWidget):
    def __init__(self, model, parent=None):
        Qt.QWidget.__init__(self, parent=parent)
        # super().__init__(parent)
        self.rga = tango.DeviceProxy(model)



        self.iconplay = Qt.QIcon.fromTheme("player_play")
        self.iconstop = Qt.QIcon.fromTheme("player_stop")

        self.modeltoPass=model

        self.fil=FilamentManager(model)

        self.maxmass = self.rga.read_attribute('MaxMass')

        # self.analogScanWidget = Qt.QWidget()
        self.mainLay = Qt.QVBoxLayout()
        self.buttonLay=Qt.QHBoxLayout()
        self.setLayout(self.mainLay)

        self.playButton = Qt.QPushButton()
        self.playButton.setIcon(self.iconplay)
        self.playButton.clicked.connect(self.startScan)

        self.buttonLay.addWidget(self.playButton)
        self.playButton.setFixedSize(75,50)
        self.playButton.setStyleSheet\
            ("border: 2px solid black;border-radius: 4px, QPushButton::hover")

        self.stopButton = TaurusCommandButton(command="StopScan")
        self.stopButton.clicked.connect(self.stopScan)
        self.stopButton.setModel(model)
        self.stopButton.setIcon(self.iconstop)
        self.buttonLay.addWidget(self.stopButton)
        self.stopButton.setFixedSize(75,50)
        self.stopButton.setStyleSheet\
            ("border: 2px solid black; border-radius: 4px, QPushButton::hover")

        self.buttonLay.addWidget(self.fil)


        self.massBox1 = Qt.QLineEdit()
        self.massBox1.setValidator(Qt.QIntValidator())
        self.massBox1.setText('1')

        self.massBox2 = Qt.QLineEdit()
        self.massBox2.setValidator(Qt.QIntValidator())
        self.massBox2.setText('2')

        self.points = Qt.QComboBox()
        self.accuracyCombo = Qt.QComboBox()
        self.sensorsBox = TaurusAttrListComboBox()
        self.sensorsBox.setModel(model + "/Sensors")

        self.gainBox = TaurusAttrListComboBox()
        self.gainBox.setModel(model + '/Gains')

        pointsTo = (2, 4, 8, 16, 32)
        for i in pointsTo:
            self.points.addItem(str(i))
        for i in range(9):
            self.accuracyCombo.addItem(str(i))

        self.sensorsBox.setModel(model + '/Sensors')
        self.gainBox.setModel(model + '/Gains')

        self.layoutForm = Qt.QFormLayout()

        self.recordButton=Qt.QPushButton()
        self.recordButton.setFixedSize(20,20)
        # self.a = record(model)
        self.recordButton.clicked.connect(self.recordFunc)

        self.layoutForm.addRow("Max Mass supported= ",
                               Qt.QLabel(str(self.maxmass.value)))
        self.layoutForm.addRow("First Mass", self.massBox1)
        self.layoutForm.addRow("Last Mass", self.massBox2)
        self.layoutForm.addRow("Points Per Mass", self.points)
        self.layoutForm.addRow("Accuracy", self.accuracyCombo)
        self.layoutForm.addRow("Sensor", self.sensorsBox)
        self.layoutForm.addRow("Gain", self.gainBox)
        self.layoutForm.addRow("Record", self.recordButton)

        # self.layoutForm.addWidget(self.playButton)
        # self.layoutForm.addWidget(self.stopButton)

        self.mainLay.addLayout(self.layoutForm)
        self.mainLay.addLayout(self.buttonLay)

        # self.analogScanWidget.show()
        # sys.exit(app.exec_())


    def recordFunc(self):

        print("OL")
        record(self.modeltoPass, "analog")


    def startScan(self):
        mass1 = int(self.massBox1.text())
        mass2 = int(self.massBox2.text())
        massDevice = self.maxmass.value

        if mass2 > massDevice:
            self.errorPoPuP(1)
        elif mass2 < mass1:
            self.errorPoPuP(2)
        else:
            self.rga.write_attribute("MassStartFrom", mass1)
            self.rga.write_attribute("MassUpTo", mass2)
            self.rga.write_attribute("PointsPerMass",
                                     int(self.points.currentText()))
            self.rga.write_attribute("Accuracy",
                                     int(self.accuracyCombo.currentText()))
            self.rga.write_attribute("DetectorSelected",
                                     self.sensorsBox.currentIndex())
            self.rga.write_attribute("GainSelected",
                                     int(self.gainBox.currentText()))

            if int(self.points.currentText()) == 1:
                self.rga.command_inout("StartBarScan")
            else:
                self.rga.command_inout("StartAnalogScan")

            self.playButton.setEnabled(False)
            self.massBox1.setEnabled(False)
            self.massBox2.setEnabled(False)
            self.points.setEnabled(False)
            self.accuracyCombo.setEnabled(False)
            self.sensorsBox.setEnabled(False)
            self.gainBox.setEnabled(False)

    def stopScan(self):
        # self.rga.command_inout("StopScan")
        # self.rga.command_inout("")
        self.playButton.setEnabled(True)
        self.massBox1.setEnabled(True)
        self.massBox2.setEnabled(True)
        self.points.setEnabled(True)
        self.accuracyCombo.setEnabled(True)
        self.sensorsBox.setEnabled(True)
        self.gainBox.setEnabled(True)
        print(self.stopButton.isChecked())

    def errorPoPuP(self, cause):
        if cause == 1:
            text = "Last mass to scan can't be higher than the" \
                   " mass supported by the device"
        else:
            text = "Ending mass can't be higher than starting mass"
        popup = QMessageBox()
        popup.setWindowTitle("ERROR")
        popup.setText(text)
        popup.setIcon(QMessageBox.Critical)
        popup.setStandardButtons(QMessageBox.Close)
        popup.exec_()





if __name__ == "__main__":

    app = Qt.QApplication(sys.argv)
    w = AnalogScanWidget("comedor/mksRGA/1")
    w.show()
    sys.exit(app.exec_())
