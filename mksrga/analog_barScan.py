"""conda create -n taurus qtpy = 1.11.13"""
import sys

from PyQt5 import Qt, QtCore

import taurus, taurus_pyqtgraph
import taurus.qt.qtgui

import tango
from taurus.qt.qtgui.input.tauruscombobox import TaurusAttrListComboBox

from PyQt5.QtWidgets import QMessageBox, QSplitter
from taurus.qt.qtgui.extra_guiqwt import taurustrend2d as t2d
from taurus.qt.qtgui.button.taurusbutton import TaurusCommandButton
from taurus_pyqtgraph import TaurusPlotDataItem

from analogScanWidget import AnalogScanWidget
class AnalogScanForm(Qt.QWidget):
    def __init__(self, model, parent=None):
        Qt.QWidget.__init__(self, parent=parent)
        # super().__init__(parent)

        self.rga = tango.DeviceProxy(model)

        masses = self.rga.read_attribute("AnalogMasses").value
        self.dialog2d = t2d.TaurusTrend2DDialog()
        self.dialog2d.setModel(model + "/AnalogPressures")
        # self.dialog2d.y=masses

        # masses = list(masses)
        print("MASSSES",masses)
        print(type(masses))
        self.tp = taurus.qt.qtgui.tpg.TaurusPlot()
        self.tp.xmodel=(model + "/AnalogMasses")
        self.tp.setModel(model + "/AnalogPressures")

        # self.tp.xModel(model+ "/AnalogMasses")
        # self.tp.getAxis("bottom").setTicks([masses])

        mainLay = Qt.QVBoxLayout()
        analogScan = AnalogScanWidget(model)

        splitter1 = QSplitter(QtCore.Qt.Horizontal)
        splitter1.addWidget(analogScan)
        splitter1.addWidget(self.tp)

        splitter2 = QSplitter(QtCore.Qt.Vertical)
        splitter2.addWidget(splitter1)
        splitter2.addWidget(self.dialog2d)

        mainLay.addWidget(splitter2)


        self.setLayout(mainLay)
        self.show()

if __name__ == "__main__":
    app = Qt.QApplication(sys.argv)
    w = AnalogScanForm("comedor/mksRGA/1")
    # w.show()
    sys.exit(app.exec_())
