import os
import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMessageBox, QDialog, QDialogButtonBox
from taurus.qt.qtgui.base import TaurusBaseWidget
from PyQt5 import Qt

import time
import threading
from taurus.qt.qtgui.button import TaurusCommandButton

import tango


class DegasManager(Qt.QWidget):

    def __init__(self, model, parent=None):
        Qt.QWidget.__init__(self, parent=parent)
        # TaurusBaseWidget.__init__(self, name='DegasManager')
        self.rga = tango.DeviceProxy(model)

        self.setFixedWidth(180)
        self.setWindowIcon(QIcon('logoRGAGUI.png'))
        self.setWindowTitle("Degas Manager")
        self.iconplay = Qt.QIcon.fromTheme("player_play")
        self.iconstop = Qt.QIcon.fromTheme("player_stop")
        print("Degass manager model==", model)
        lay = Qt.QVBoxLayout()
        lay1_1 = Qt.QHBoxLayout()


        """TO DO: add default values"""

        self.starPower = Qt.QLineEdit()
        self.starPower.setMaxLength(2)
        self.starPower.setValidator(Qt.QIntValidator(1, 100))
        self.starPower.setText("3")

        self.endPower = Qt.QLineEdit()
        self.endPower.setMaxLength(2)
        self.endPower.setValidator(Qt.QIntValidator(1, 100))
        self.endPower.setText("5")

        self.rampUp = Qt.QLineEdit()
        self.rampUp.setMaxLength(3)
        self.rampUp.setValidator(Qt.QIntValidator(1, 600))
        self.rampUp.setText("10")

        self.rampDown = Qt.QLineEdit()
        self.rampDown.setMaxLength(3)
        self.rampDown.setValidator(Qt.QIntValidator(1, 900))
        self.rampDown.setText("10")


        self.dwell = Qt.QLineEdit()
        self.dwell.setMaxLength(4)
        self.dwell.setValidator(Qt.QIntValidator(1, 1800))
        self.dwell.setText("10")

        formLayout = Qt.QFormLayout()

        formLayout.addRow("Start Power", self.starPower)
        formLayout.addRow("End Power", self.endPower)
        formLayout.addRow("Ramp Up Time", self.rampUp)
        formLayout.addRow("Dwell Time", self.rampDown)
        formLayout.addRow("Ramp Down Time", self.dwell)

        self.buttonGo = Qt.QPushButton()
        self.buttonGo.clicked.connect(self.startDegas)
        self.buttonGo.setIcon(self.iconplay)

        # self.buttonAbort = Qt.QPushButton()
        # self.buttonAbort.clicked.connect(self.abortDegas)
        # self.buttonAbort.setIcon(self.iconstop)

        lay1_1.addWidget(self.buttonGo)
        # lay1_1.addWidget(self.buttonAbort)

        # self.buttonAbort.setDisabled(True)

        lay.addLayout(formLayout)
        lay.addLayout(lay1_1)
        self.setLayout(lay)

        self.th = threading.Thread(target=self.degasState)
        self.degasAbort = threading.Event()

        """dialog"""
        self.dlg = QDialog()
        self.dlg.isModal()
        self.layout = Qt.QVBoxLayout()
        self.dlg.setWindowTitle("Degasing")
        self.dlg.setWindowIcon(QIcon('logoRGAGUI.png'))
        self.abortButton = Qt.QPushButton("Abort")
        self.degasLabel = Qt.QLabel()
        self.dlg.setLayout(self.layout)
        self.layout.addWidget(self.degasLabel)
        self.layout.addWidget(self.abortButton)
        self.abortButton.setIcon(self.iconstop)
        self.abortButton.clicked.connect(self.degasAborted)

        self.show()
        sys.exit(app.exec_())

    def startDegas(self):

        if int(self.starPower.text())<int(self.endPower.text()):
            self.dlg.show()
            self.buttonGo.setDisabled(True)
            startPower =self.starPower.text()
            print(type(startPower))
            endPower = self.endPower.text()
            rampUP = self.rampUp.text()
            rampDown =self.rampDown.text()
            dwell =self.dwell.text()
            self.rga.command_inout('FIL_ON')
            time.sleep(1)
            cmd = startPower + " " + endPower + " " + rampUP +\
                  " " + rampDown + " " + dwell
            print(cmd)
            self.rga.command_inout("StartDegas", cmd )
            self.th.start()

        else:
            self.errorPoPuP()

    def degasState(self):
        print("ok")
        emptymessage=0
        while not self.degasAbort.isSet():
            if emptymessage<3:
                a = self.rga.read_attribute("Async").value
                if "DegasReading" in a:
                    self.degasLabel.setText(a)
                    self.degasLabel.update()
                # a = a.split('\r\n')
                # for i in a:
                #     print(i)
                    time.sleep(0.5)
                if "Complete" in a:
                    self.degasLabel.setText("Degas Completed")
                    self.degasLabel.update()
                    self.degasFinished()

                if a==' ':
                    emptymessage +=1


    def degasAborted(self):
        self.degasLabel.setText("Degas Aborted")
        self.degasLabel.update()
        self.degasFinished()

    def degasFinished(self):
        self.degasAbort.set()
        self.abortButton.setText("Close")
        self.abortButton.update()
        self.abortButton.toggle()
        self.abortButton.clicked.connect(self.dlg.close)
        self.rga.command_inout("StopDegas")
        a = self.rga.read_attribute("Async").value
        while a:
            print(a)
            a = self.rga.read_attribute("Async").value
            print(a)
        self.buttonGo.setDisabled(False)
        print("Degas Aborted")

    def errorPoPuP(self):

        text = "StartPower must be lower thant EndPower."
        popup = QMessageBox()
        popup.setWindowTitle("ERROR")
        popup.setText(text)
        popup.setIcon(QMessageBox.Warning)
        popup.setStandardButtons(QMessageBox.Close)
        popup.exec_()

if __name__ == "__main__":
    # Initialize a Qt application (Qt will crash if you do not do this first)
    app = Qt.QApplication(sys.argv)

    # instantiate the widget
    w = DegasManager('comedor/mksRGA/1')

    w.setWindowTitle("Configure Degas")

    # show it (if you do not show the widget, it won't be visible)
    w.show()

    # Initialize the Qt event loop (and exit when we close the app)
    """to check if exit rises up to the main gui"""
    # sys.exit(app.exec_())
