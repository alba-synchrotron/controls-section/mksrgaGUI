import sys
from taurus.qt.qtgui.base import TaurusBaseWidget

from PyQt5 import Qt
from taurus.qt.qtgui.button import TaurusCommandButton
import tango
from PyQt5.QtWidgets import QDialog, QMessageBox


class RunDiagnostic(Qt.QWidget, TaurusBaseWidget):
    def __init__(self, model, parent=None):

        Qt.QWidget.__init__(self, parent=parent)
        # TaurusBaseWidget.__init__(self, name="RunDiagnostic")

        self.rga = tango.DeviceProxy(model)

        # self.lay = Qt.QHBoxLayout(self)
        # self.buttonDiag = Qt.QPushButton()
        # self.buttonDiag.setModel(model)
        #
        # s = self.buttonDiag.commandExecuted.connect(self.set_text)
        # print(s)
        #
        # self.textBox = Qt.QLabel(self)
        #
        # self.lay.addWidget(self.buttonDiag)
        # self.lay.addWidget(self.textBox)

        self.checkFil()
        # self.show()

    def checkFil(self):
        a = self.rga.read_attribute("FilamentState")
        print(a.value)
        if a.value == "ON":
            pass
        else:
            popup = QMessageBox()
            popup.setWindowTitle('Filament Off')
            popup.setText("Filament is off.\n Do you want to continue anyway?")
            popup.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            popup.setDefaultButton(QMessageBox.Cancel)
            popup.buttonClicked.connect(self.decission)
            popup.exec_()

    def decission(self, i):

        print(i.text())
        if i.text() == '&OK':
            print("22")
            diag = self.rga.read_attribute("Diagnostic")
            # self.label.setText(self.diag.value)
            # self.widget.show()

            print(diag.value)
            popup = QMessageBox()

            popup.setWindowTitle('Diagnostic')
            if "Fail" in diag.value:
                icon = QMessageBox.Warning
            else:
                icon = QMessageBox.Information
            popup.setIcon(icon)
            popup.setText(diag.value)
            popup.exec_()

        elif i.text() == '&CANCEL':
            pass
        # sys.exit(app.exec_())


if __name__ == '__main__':
    app = Qt.QApplication(sys.argv)
    w = RunDiagnostic("comedor/mksRGA/1")
    # w.setModel("comedor/mksRGA/1")
    # w.setWindowTitle("Diagnostic")
    # w.show()
    # Initialize the Qt event loop (and exit when we close the app)
    sys.exit(app.exec_())
