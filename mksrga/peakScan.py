import sys

from PyQt5 import Qt, QtGui, QtCore
# from PyQt5.QtCore import pyqtSignal
# PyQt5.QtCore.Signal=PyQt5.QtCore.pyqtSignal
from addMassForm import AddMassForm

import taurus, taurus_pyqtgraph as tp
import taurus.qt.qtgui
from taurus.qt.qtgui.button.taurusbutton import TaurusCommandButton
import tango
from PyQt5.QtWidgets import QFrame, QSplitter, QMessageBox
from taurus.external.qt import Qt
from taurus.qt.qtgui.display import QLed
from filament_2 import FilamentManager

from recordWidget import ScanRecord as record
class PeakScanWidget(Qt.QWidget):
    def __init__(self, model,rga, parent=None):
        # super().__init__(parent)
        Qt.QWidget.__init__(self, parent=parent)

        self.rga =rga
        self.modeltoPass=model

        self.massForm = AddMassForm(self.rga)
        self.mainlay = Qt.QHBoxLayout()

        self.fil=FilamentManager(self.rga)
        self.fil.setFixedSize(120,100)

        self.iconplay = Qt.QIcon.fromTheme("player_play")
        self.iconstop = Qt.QIcon.fromTheme("player_stop")

        self.playButton = TaurusCommandButton(command='StartPeakScan')
        self.playButton.clicked.connect(self.startScan)
        self.playButton.setIcon(self.iconplay)
        self.playButton.setModel(model)
        self.playButton.setFixedSize(100, 50)
        self.playButton.setStyleSheet \
            ("border: 1px solid black;border-radius: 3px,QPushButton::hover")

        self.stopButton = TaurusCommandButton(command='StopScan')
        self.stopButton.clicked.connect(self.stopScan)
        self.stopButton.setIcon(self.iconstop)
        self.stopButton.setModel(model)
        self.stopButton.setFixedSize(100, 50)
        self.stopButton.setStyleSheet \
            ("border: 1px solid black; border-radius: 3px,QPushButton::hover")

        self.recordButton=Qt.QPushButton()
        self.recordButton.clicked.connect(self.recordFunc)

        self.tt = taurus.qt.qtgui.tpg.TaurusTrend()
        self.tt.setModel(model + "/PeakPressures")

        splitter1 = QSplitter(QtCore.Qt.Vertical)
        splitter1_1 = QSplitter(QtCore.Qt.Horizontal)
        splitter2 = QSplitter(QtCore.Qt.Vertical)
        splitter1.addWidget(self.massForm)
        splitter1_1.addWidget(self.playButton)
        splitter1_1.addWidget(self.stopButton)
        splitter1_1.addWidget(self.fil)
        splitter1_1.addWidget(self.recordButton)

        splitter2.addWidget(self.tt)
        splitter1.setStretchFactor(1, 0)
        splitter1.addWidget(splitter1_1)
        splitter1.setSizes([125, 150])

        self.mainlay.addWidget(splitter1)
        self.mainlay.addWidget(splitter2)
        self.setLayout(self.mainlay)
        # QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))
        # self.setGeometry(300, 300, 300, 200)

        self.show()

    def recordFunc(self):
        print("OL")
        a=record(self.modeltoPass, "peak")
        a.show()


    def startScan(self):
        print("AA")
        self.massForm.setDisabled(True)

    def stopScan(self):
        print("BB")
        self.massForm.setDisabled(False)

    def closeEvent(self, event):
        self.rga.command_inout("StopScan")
        self.rga.command_inout("PeakScanRemoveAll")
        event.accept()


if __name__ == "__main__":
    app = Qt.QApplication(sys.argv)
    w = PeakScanWidget("comedor/mksRGA/1")
    w.show()
    sys.exit(app.exec_())
