import sys
import time

from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, \
    QFileDialog, QDialog, QPushButton, QVBoxLayout, QLabel, QHBoxLayout
from PyQt5.QtGui import QIcon
import tango


import datetime
import numpy as np
from PyQt5 import Qt
from recordController import RecordController as rController
from PyQt5.QtCore import QObject, QThread, pyqtSignal
class ScanRecord(Qt.QWidget):

    def __init__(self, model, mode,parent=None):
        Qt.QWidget.__init__(self, parent=parent)

        self.mode=mode
        # app = QApplication(sys.argv)
        super().__init__()

        self.rga = tango.DeviceProxy(model)
        self.counter=10
        self.fileName = self.saveFileDialog()
        print(self.fileName)
        self.model=model
        self.mode=mode
        self.th=QThread()
        self.recordObject = rController(self.model, self.mode, self.fileName)
        self.recordObject.moveToThread(self.th)
        # self.recordObject.finished.connect(self.recordObject.deleteLater())
        self.recordObject.recorded.connect(self.updateLabel)
        self.th.started.connect(self.recordObject.startRecord)


        # with open(self.filename, "w+") as file:
        #     file.write(str(datetime.datetime.now())+"\n" )
        #     file.write(self.rga.read_attribute("GeneralInfo").value)
        #     file.close()

        # self.commentDialog = QWidget()
        self.layout = QVBoxLayout()
        self.buttonLayout = QHBoxLayout()
        self.commentLabel = QLabel("Comment")
        self.commentBox = QLineEdit()
        self.commentBox.returnPressed.connect(self.recordComment)
        self.scanLabel = QLabel("Scan")
        self.scanBox = QLabel()
        # self.scanBox = QLineEdit()
        # self.scanBox.setReadOnly(True)

        self.recordButton = QPushButton("RECORD")
        self.recordButton.clicked.connect(self.record)
        self.exitButton = QPushButton("EXIT")
        self.exitButton.clicked.connect(self.stopRecord)


        self.layout.addWidget(self.scanLabel)
        self.layout.addWidget(self.scanBox)
        self.layout.addWidget(self.commentLabel)
        self.layout.addWidget(self.commentBox)
        self.buttonLayout.addWidget(self.recordButton)
        self.buttonLayout.addWidget(self.exitButton)
        self.layout.addLayout(self.buttonLayout)
        # self.commentDialog.setLayout(self.layout)
        # self.saveFileDialog()


        self.setLayout(self.layout)


        self.show()
        sys.exit(app.exec_())

    def record(self):
        self.recordObject.stopRecord.clear()
        self.th.start()
        self.recordButton.setText("PAUSE")
        self.recordButton.clicked.connect(self.resume)

    def resume(self):
        self.recordObject.stopRecord.clear()

    def stopRecord(self):

        self.recordObject.stopRecord.set()


    def recordComment(self):
        a=self.commentBox.text()
        self.scanBox.setText(a)
        self.recordObject.recordComment(a)
        self.commentBox.clear()

    def updateLabel(self,a):
        self.scanBox.setText(a)




    def saveFileDialog(self):
        # options = QFileDialog.Options()
        # options |= QFileDialog.DontUseNativeDialog
        self.fileName, _ = QFileDialog.getSaveFileName \
            (self,"QFileDialog.getSaveFileName()","","Text Files (*.txt)")

        if self.fileName:
            return self.fileName


if __name__ == '__main__':
    app = QApplication(sys.argv)
    model='comedor/mksRGA/1'
    ex = ScanRecord(model, "peak")
    ex.show()
    sys.exit(app.exec_())
