import sys
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIntValidator
from taurus.qt.qtgui.input import TaurusValueLineEdit, TaurusAttrListComboBox\

from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.display import TaurusLabel
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QCheckBox,QComboBox
from taurus.qt.qtgui.button.taurusbutton import TaurusCommandButton

class SelectMassWiget(QWidget):
    def __init__(self, number,model, parent=None):
        super().__init__(parent)
        app = TaurusApplication(sys.argv, cmd_line_parser=None, )

        self.panel = QWidget()
        self.layout = QHBoxLayout()
        self.panel.setLayout(self.layout)

        self.labelNumber = QLabel()
        self.massLabel = TaurusValueLineEdit()
        self.detectorCombo = TaurusAttrListComboBox()
        self.gainsCombo = TaurusAttrListComboBox()
        self.accuracyCombo = QComboBox()
        self.addMassCheck = QCheckBox('Added')



        self.labelNumber.setText(str(number))
        self.massLabel.setMaxLength(2)
        self.massLabel.setToolTip('Mass to scan')
        self.detectorCombo.setModel(model + '/Sensors')
        self.detectorCombo.setToolTip('Sensor to use')
        self.gainsCombo.setModel(model + '/Gains')
        self.gainsCombo.setToolTip('Gain to use')

        for i in range(9):
            self.accuracyCombo.addItem(str(i))
        self.accuracyCombo.setToolTip("Accuracy for this mass")

        self.addMassCheck.setToolTip('Add mass to scan')
        self.addMassCheck.setChecked(False)
        self.addMassCheck.stateChanged.connect(lambda:self.addToScan())

        self.layout.addWidget(self.labelNumber)
        self.layout.addWidget(self.massLabel)
        self.layout.addWidget(self.detectorCombo)
        self.layout.addWidget(self.gainsCombo)
        self.layout.addWidget(self.accuracyCombo)
        self.layout.addWidget(self.addMassCheck)

        self.panel.show()
        sys.exit(app.exec_())
    def addToScan(self):
        if self.addMassCheck.isChecked():
            self.massLabel.setEnabled(False)
            self.gainsCombo.setEnabled(False)
            self.detectorCombo.setEnabled(False)
            self.accuracyCombo.setEnabled(False)

            massToScan=int(self.massLabel.text())
            detectorToUse=self.detectorCombo.currentIndex()+1
            gainToUse=self.gainsCombo.currentIndex()+1
            accuracyToUse=self.accuracyCombo.currentIndex()+1

        else:
            self.massLabel.setEnabled(True)
            self.gainsCombo.setEnabled(True)
            self.detectorCombo.setEnabled(True)
            self.accuracyCombo.setEnabled(True)

if __name__ == "__main__":
    app = TaurusApplication()
    w = SelectMassWiget(1, 'comedor/mksRGA/1')
    w.show()
    sys.exit(app.exec_())
