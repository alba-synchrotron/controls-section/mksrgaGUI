#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', ]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', ]

setup(
    author="Lluis Gines",
    author_email='lgines@cells.es',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="mksrga is a collection of tools and widgets for MKS Resuidual Gas Analyzers Microvision IP",
    entry_points={
        'console_scripts': [
            'mksrga=mksrga.cli:main',
        ],
    },
    install_requires=requirements,
    license="GPLv3+",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='mksrga',
    name='mksrga',
    packages=find_packages(include=['mksrga']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/LluisG/mksrga',
    version='0.1.0',
    zip_safe=False,
)
